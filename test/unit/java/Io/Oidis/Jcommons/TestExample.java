/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons;

import Io.Oidis.Assert;
import Io.Oidis.UnitTest;

import java.util.ArrayList;
import java.util.List;

class MockExceptionEmitter implements Runnable {
    public void run() {
        List<Integer> someList = new ArrayList<Integer>();
        someList.add(0);
        for (Integer item : someList) {
            System.out.println(10 / item);
        }
    }
}

public class TestExample extends UnitTest {

    private static final String testDataPath = "build/test_data";

    public void before() {
        System.out.println("before");
    }

    public void after() {
        System.out.println("after");
    }

    public void setUp() {
        System.out.println("setUp");
    }

    public void tearDown() {
        System.out.println("tearDown");
    }

    public void __ignoreTestCase() throws Exception {
        System.out.println("THIS IS SOME TEST");
    }

    public void testCase1() throws Exception {
        System.out.println("testCase1");
        Assert.equal(1, 1);
    }

    public void testCase2() throws Exception {
        System.out.println("testCase2");
        Assert.throwsException(new MockExceptionEmitter());
    }
}

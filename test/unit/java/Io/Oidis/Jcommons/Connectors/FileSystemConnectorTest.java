/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Connectors;

import Io.Oidis.Assert;
import Io.Oidis.Jcommons.Connectors.Args.ArchiveOptions;
import Io.Oidis.UnitTest;

import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.*;

public class FileSystemConnectorTest extends UnitTest {

    private static final String testDataPath = "build/test_data";

    public void __IgnoretestUnpack() throws Exception {
        Unpack("resource/target.zip", new ArchiveOptions().Output(testDataPath));
        Assert.equal(Exists(testDataPath), true);
        Delete(testDataPath);
        Assert.equal(Exists(testDataPath), false);
    }
}

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Executors;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ThreadExecutor {

    private static ThreadExecutor instance;
    private ThreadPoolExecutor executor;

    private ThreadExecutor() {
        /* checkstyle:disable: MagicNumber */
        executor = new ThreadPoolExecutor(1, 4, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());
        /* checkstyle:enable */
    }

    public static ThreadExecutor getInstance() {
        if (instance == null) {
            instance = new ThreadExecutor();
        }
        return instance;
    }

    public synchronized void Synchronize(final Runnable $task) {
        $task.run();
    }

    public void Execute(final Runnable $task) {
        executor.execute($task);
    }
}

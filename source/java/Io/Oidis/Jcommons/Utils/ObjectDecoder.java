/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObjectDecoder { // checkstyle:disable-line

    public static String Base64(final String $input) {
        if ($input == null) {
            return "";
        }
        try {
            return new String(Base64.getDecoder().decode(
                    $input.replace("-", "+")
                            .replace("_", "/")
                            .replace(".", "=")
                            .getBytes("utf-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T JSON(final Object $input, final Class<T> $tClass) {
        if ($input == null || $tClass == null) {
            return null;
        }
        try {
            if ($input instanceof String) {
                Pattern pattern = Pattern.compile("((?:^\\s*)|(?:([{]|,)\\s*))((?:\\$)?[\\w_]+)\\s*:");
                Matcher matcher = pattern.matcher($input.toString());
                StringBuffer parsableStringBuffer = new StringBuffer();
                while (matcher.find()) {
                    matcher.appendReplacement(parsableStringBuffer, "$1\\\"$3\\\": ");
                }
                matcher.appendTail(parsableStringBuffer);
                return new ObjectMapper().readValue(parsableStringBuffer.toString(), $tClass);
            } else {
                return new ObjectMapper().convertValue($input, $tClass);
            }
        } catch (Exception e) {
            return null;
        }
    }
}

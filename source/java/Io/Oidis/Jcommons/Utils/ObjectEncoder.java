/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class ObjectEncoder { // checkstyle:disable-line

    public static String Base64(final String $input) {
        return Base64($input, false);
    }

    public static String Base64(final String $input, final Boolean $urlSafe) {
        if ($input == null) {
            return "";
        }
        try {
            String encoded = Base64.getEncoder().encodeToString($input.getBytes("utf-8"));
            if ($urlSafe) {
                encoded = encoded
                        .replace("+", "-")
                        .replace("/", "_")
                        .replace("=", ".");
            }
            return encoded;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String JSON(final Object $input) {
        try {
            if ($input == null) {
                return null;
            }
            return new ObjectMapper().writeValueAsString($input);
        } catch (Exception e) {
            return null;
        }
    }
}

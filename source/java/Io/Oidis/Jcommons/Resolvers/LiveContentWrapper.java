/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Resolvers;

import Io.Oidis.Jcommons.Exceptions.WuiIllegalArgumentException;
import Io.Oidis.Jcommons.Resolvers.Protocols.LiveContentProtocol;
import Io.Oidis.Jcommons.Utils.ObjectEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class LiveContentWrapper {

    private static final Logger logger = LogManager.getLogger(LiveContentWrapper.class);

    private LiveContentWrapper() {
    }

    public static LiveContentProtocol InvokeMethod(final LiveContentProtocol $protocol) {
        try {
            String name = $protocol.name;
            String className = name.substring(0, name.lastIndexOf("."));
            String methodName = name.substring(name.lastIndexOf(".") + 1, name.length());
            List<Argument> arguments = Argument.getArgumentList($protocol.args);
            Class<?> targetClass;
            try {
                targetClass = Class.forName(className);
            } catch (ClassNotFoundException exception) {
                targetClass = null;
            }
            if (targetClass != null) {
                Method[] methods = targetClass.getDeclaredMethods();
                List<Method> callableMethods = new ArrayList<>();
                for (Method method : methods) {
                    if (method.getName().equals(methodName) && Argument.IsMethodInvokableWithArguments(method, arguments)) {
                        callableMethods.add(method);
                    }
                }
                if (callableMethods.size() == 1) {
                    Method method = callableMethods.get(0);
                    Object[] parameters = Argument.ConvertArgumentsToParameterTypes(arguments, method);
                    LiveContentProtocol liveContentProtocol = new LiveContentProtocol();
                    liveContentProtocol.name = name;
                    liveContentProtocol.returnValue = ObjectEncoder.JSON(method.invoke(null, parameters));
                    return liveContentProtocol;
                } else if (!callableMethods.isEmpty()) {
                    List<String> ambiguousParametersList = new ArrayList<>();
                    for (Method method : callableMethods) {
                        ambiguousParametersList.add(methodName + Arrays.asList(method.getParameterTypes()).toString()
                                .replace("[", "(")
                                .replace("]", ")")
                        );
                    }
                    throw new WuiIllegalArgumentException("Method invocation failed.", "Method \"" + methodName + "\" in class \""
                            + className + "\" was invoked ambiguously. Conflicts : "
                            + String.join(" and ", ambiguousParametersList) + ".");
                } else {
                    throw new WuiIllegalArgumentException("Method invocation failed.", "Required method \"" + methodName + "\" "
                            + "does not exist in class \"" + className + "\" .");
                }
            } else {
                throw new WuiIllegalArgumentException("Method invocation failed.", "Required method \"" + methodName + "\" "
                        + "does not exist in class \"" + className + "\" .");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new WuiIllegalArgumentException("Method invocation failed.", e.getMessage());
        }
    }
}

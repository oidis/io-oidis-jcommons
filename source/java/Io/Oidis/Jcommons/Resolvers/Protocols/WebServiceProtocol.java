/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Resolvers.Protocols;

public class WebServiceProtocol implements Cloneable {
    /* checkstyle:disable: JavadocVariable|VisibilityModifier */
    public Integer id;
    public Integer clientId;
    public Integer status;
    public String type;
    public String origin;
    public String data;
    public String token;
    /* checkstyle:enable */

    public WebServiceProtocol clone() {
        try {
            return (WebServiceProtocol) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}

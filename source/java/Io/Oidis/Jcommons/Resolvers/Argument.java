/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Resolvers;

import Io.Oidis.Jcommons.Exceptions.WuiIllegalArgumentException;
import Io.Oidis.Jcommons.Utils.ObjectDecoder;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class Argument {
    private Object value;
    private List<Class<?>> assignableTypes;

    public Object getValue() {
        return value;
    }

    public List<Class<?>> getAssignableTypes() {
        return assignableTypes;
    }

    Object Typecast(final Class<?> $class) {
        if (isAssignable($class)) {
            if (value instanceof Double && ($class == Float.class || $class == Float.TYPE)) {
                return ((Double) value).floatValue();
            } else if (value instanceof Number || value instanceof String) {
                return value;
            } else if (value instanceof ArrayList) {
                if ($class == Object[].class) {
                    return ObjectDecoder.JSON(value, $class);
                }
                return value;
            } else if (value instanceof Map) {
                return ObjectDecoder.JSON(value, $class);
            } else {
                return null;
            }
        } else {
            throw new RuntimeException("Value \"" + value + "\" is not assignable to type \"" + $class + "\" .");
        }
    }

    boolean isAssignable(final Class<?> $class) {
        for (Class<?> type : assignableTypes) {
            if (type.isAssignableFrom($class)) {
                return true;
            }
        }
        return false;
    }

    static List<Argument> getArgumentList(final String $jsonArray) {
        List<Argument> argumentList = new ArrayList<>();
        if ($jsonArray.length() < 3) {
            return argumentList;
        }
        Object[] argList = ObjectDecoder.JSON($jsonArray, Object[].class);
        if (argList == null) {
            throw new WuiIllegalArgumentException("Failure getting arguments from JSON array.",
                    "JSON array is not parsable : " + $jsonArray + ".");
        }
        for (Object arg : argList) {
            if (arg == null) {
                argumentList.add(new Argument(null, Object.class));
            } else if (arg instanceof String) {
                argumentList.add(new Argument(arg, String.class));
            } else if (arg instanceof Integer) {
                argumentList.add(new Argument(arg, Integer.class, Integer.TYPE,
                        Long.class, Long.TYPE));
            } else if (arg instanceof Long) {
                argumentList.add(new Argument(arg, Long.class, Long.TYPE));
            } else if (arg instanceof Double) {
                argumentList.add(new Argument(arg, Double.class, Double.TYPE,
                        Float.class, Float.TYPE));
            } else if (arg instanceof ArrayList) {
                argumentList.add(new Argument(arg, ArrayList.class, Object[].class));
            }
        }
        return argumentList;
    }

    static boolean IsMethodInvokableWithArguments(final Method $method, final List<Argument> $arguments) {
        if ($arguments.size() != $method.getParameterCount()) {
            return false;
        }
        Class<?>[] parameterTypes = $method.getParameterTypes();
        for (int i = 0; i < parameterTypes.length; i++) {
            if (!$arguments.get(i).isAssignable(parameterTypes[i])) {
                return false;
            }
        }
        return true;
    }

    static Object[] ConvertArgumentsToParameterTypes(final List<Argument> $arguments, final Method $method) {
        Class<?>[] parameterTypes = $method.getParameterTypes();
        List<Object> parameters = new ArrayList<>();
        for (int i = 0; i < parameterTypes.length; i++) {
            if (!$arguments.get(i).isAssignable(parameterTypes[i])) {
                throw new WuiIllegalArgumentException("Conversion of argument to parameter type failed.", "Argument of type "
                        + $arguments.get(i).getClass() + " is not assignable to type " + parameterTypes[i] + ".");
            }
            parameters.add($arguments.get(i).Typecast(parameterTypes[i]));
        }
        return parameters.toArray();
    }

    private Argument(final Object $value, final Class<?>... $assignableTypes) {
        value = $value;
        assignableTypes = Arrays.asList($assignableTypes);
    }
}

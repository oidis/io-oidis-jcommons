/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Interfaces;

import Io.Oidis.Jcommons.Resolvers.Protocols.WebServiceProtocol;

public interface IServer {
    void send(Integer $clientId, String $data);
    void resolve(WebServiceProtocol $data);
}

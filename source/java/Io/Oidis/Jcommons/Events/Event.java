/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Events;

import java.util.function.Consumer;

public class Event {
    private String type;
    private String owner;
    private Consumer<Object[]> event;

    public Event(final String $owner, final String $type, final Consumer<Object[]> $event) {
        this.type = $type;
        this.owner = $owner;
        this.event = $event;
    }

    public Consumer<Object[]> getEvent() {
        return event;
    }

    public String getOwner() {
        return owner;
    }

    public String getType() {
        return type;
    }
}

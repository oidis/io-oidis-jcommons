/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Events;

import Io.Oidis.Jcommons.Executors.ThreadExecutor;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

public final class EventsManager {

    private static EventsManager instance;
    private Map<String, Queue<Event>> listenersPerEventType;

    private EventsManager() {
        listenersPerEventType = new ConcurrentHashMap<>();
    }

    public static EventsManager getInstance() {
        if (instance == null) {
            instance = new EventsManager();
        }
        return instance;
    }

    public void setEvent(final String $type, final Consumer<Object[]> $handler) {
        setEvent(null, $type, $handler);
    }

    public void setEvent(final String $owner, final String $type, final Consumer<Object[]> $handler) {
        setEvent(new Event($owner, $type, $handler));
    }

    public void setEvent(final Event $event) {
        listenersPerEventType.putIfAbsent($event.getType(), new ConcurrentLinkedQueue<>());
        listenersPerEventType.get($event.getType()).add($event);
    }

    public void FireEvent(final String $type) {
        FireEvent(null, $type, false);
    }

    public void FireEvent(final String $type, final Object[] $arguments) {
        FireEvent(null, $type, false, $arguments);
    }

    public void FireEvent(final String $owner, final String $type) {
        FireEvent($owner, $type, false);
    }

    public void FireEvent(final String $owner, final String $type, final Object[] $arguments) {
        FireEvent($owner, $type, false, $arguments);
    }

    public void FireEvent(final String $owner, final String $type, final boolean $async, final Object... $arguments) {
        Queue<Event> events = listenersPerEventType.get($type);
        if (events != null) {
            events.forEach(event -> {
                if ($owner == null || $owner.equals(event.getOwner())) {
                    if ($async) {
                        ThreadExecutor threadExecutor = ThreadExecutor.getInstance();
                        threadExecutor.Execute(() -> event.getEvent().accept($arguments));
                    } else {
                        event.getEvent().accept($arguments);
                    }
                }
            });
        }
    }
}

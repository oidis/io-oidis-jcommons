/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Enums;

public enum ResponseStatusType {

    /* checkstyle:disable: JavadocVariable */
    OK(200),
    CONTINUE(201),
    INTERNAL_ERROR(500),
    NOT_AUTHORIZED(401);
    /* checkstyle:enable */

    private final int statusCode;

    ResponseStatusType(final int $statusCode) {
        statusCode = $statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}

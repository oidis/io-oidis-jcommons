/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Services;

import Io.Oidis.Jcommons.Connectors.RuntimeConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.ExtractJarResource;
import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.Exists;
import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.Unpack;
import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.Delete;
import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.Read;
import static Io.Oidis.Jcommons.Connectors.RuntimeConnector.Destroy;
import static Io.Oidis.Jcommons.Connectors.RuntimeConnector.getProcessIds;
import static Io.Oidis.Jcommons.Connectors.RuntimeConnector.Run;

public class ConnectorService {

    private static final Logger logger = LogManager.getLogger(ConnectorService.class);
    private Path tempPath = Paths.get(System.getProperty("java.io.tmpdir") + "/com-wui-framework-jcommons");
    private String processName;
    private String projectName;

    public ConnectorService() throws IOException {
        String filename = "target.zip";
        String targetPath = this.tempPath + filename;
        if (!Exists(this.tempPath + "/target")) {
            ExtractJarResource(filename, targetPath);
            Unpack(targetPath);
            Delete(targetPath);
        }
        this.projectName = parseProjectName(this.tempPath + "/target/target/wuirunner.config");
        this.processName = this.projectName + " Connector(.*)";
        if (RuntimeConnector.getProcessIds(processName).isEmpty()) {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    Stop();
                } catch (IOException e) {
                    throw new RuntimeException("Kill of processes '" + this.processName + "' failed.\nCause : " + e);
                }
            }));
        }
    }

    public boolean Start() {
        try {
            if (RuntimeConnector.getProcessIds(processName).isEmpty()) {
                Run(tempPath + "/target/" + projectName + ".exe");
            } else {
                logger.debug("Connector is already running - start skipped.");
            }
            return true;
        } catch (IOException e) {
            logger.error("Error starting connector.\nCause : " + e);
            return false;
        }
    }

    public void Stop() throws IOException {
        if (processName != null) {
            Run(tempPath + "/target/" + projectName + ".exe", "--stop", true);
            Set<Integer> processIds = getProcessIds(this.processName);
            for (Integer pid : processIds) {
                Destroy(pid);
            }
        }
    }

    private String parseProjectName(final String $runnerConfigPath) throws IOException {
        if (Exists($runnerConfigPath)) {
            String runnerConfig = Read($runnerConfigPath);
            Pattern pattern = Pattern.compile("<!ENTITY window.title(.*?)\"(.*?)\">");
            Matcher matcher = pattern.matcher(runnerConfig);
            if (matcher.find()) {
                return matcher.group(2);
            } else {
                throw new IOException("Error parsing project name.\nCause : wuirunner.config doesn't contain ENTITY 'window.title'.");
            }
        }
        throw new IOException("Error parsing project name.\nCause : wuirunner.config doesn't exist at path : " + $runnerConfigPath + ".");
    }
}

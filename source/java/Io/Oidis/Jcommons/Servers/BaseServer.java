/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Servers;

import Io.Oidis.Jcommons.Enums.ResponseStatusType;
import Io.Oidis.Jcommons.Events.EventsManager;
import Io.Oidis.Jcommons.Exceptions.WuiIllegalArgumentException;
import Io.Oidis.Jcommons.Executors.ThreadExecutor;
import Io.Oidis.Jcommons.Interfaces.IServer;
import Io.Oidis.Jcommons.Resolvers.LiveContentWrapper;
import Io.Oidis.Jcommons.Resolvers.Protocols.LiveContentProtocol;
import Io.Oidis.Jcommons.Resolvers.Protocols.WebServiceProtocol;
import Io.Oidis.Jcommons.Utils.ObjectDecoder;
import Io.Oidis.Jcommons.Utils.ObjectEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public abstract class BaseServer implements IServer {

    private static Logger logger = LogManager.getLogger(BaseServer.class);

    public BaseServer() {

    }

    @Override
    public void send(final Integer $clientId, final String $data) {
        ThreadExecutor threadExecutor = ThreadExecutor.getInstance();
        threadExecutor.Execute(() -> {
            try {
                logger.trace("Received data : " + $data + " from client with id \"" + $clientId + "\".");
                WebServiceProtocol webServiceProtocol = ObjectDecoder.JSON($data, WebServiceProtocol.class);
                if (webServiceProtocol != null && webServiceProtocol.type != null
                        && webServiceProtocol.type.startsWith("LiveContentWrapper.")) {
                    webServiceProtocol.clientId = $clientId;
                    webServiceProtocol.status = null;
                    LiveContentProtocol liveContentProtocol =
                            ObjectDecoder.JSON(ObjectDecoder.Base64(webServiceProtocol.data), LiveContentProtocol.class);
                    if (liveContentProtocol != null) {
                        liveContentProtocol.args = ObjectDecoder.Base64(liveContentProtocol.args);
                        if ("LiveContentWrapper.AddEventHandler".equals(webServiceProtocol.type)) {
                            if (liveContentProtocol.name != null && liveContentProtocol.name.length() > 0) {
                                String[] parts = liveContentProtocol.name.split("\\.");
                                String event = parts[parts.length - 1];
                                int splitIndex = liveContentProtocol.name.lastIndexOf(".");
                                String owner = splitIndex == -1 ? null : liveContentProtocol.name.substring(0, splitIndex);
                                webServiceProtocol.status = ResponseStatusType.OK.getStatusCode();
                                WebServiceProtocol eventProtocol = webServiceProtocol.clone();
                                LiveContentProtocol eventArgs = liveContentProtocol.clone();
                                EventsManager.getInstance().setEvent(owner, event, (Object... $argument) -> {
                                    eventArgs.args = ObjectEncoder.Base64(ObjectEncoder.JSON($argument));
                                    eventProtocol.type = "LiveContentWrapper.FireEvent";
                                    eventProtocol.data = ObjectEncoder.Base64(ObjectEncoder.JSON(eventArgs));
                                    threadExecutor.Synchronize(() -> resolve(eventProtocol));
                                });
                                webServiceProtocol.data = "OK";
                            } else {
                                throw new WuiIllegalArgumentException("AddEventHandler failed.", "Protocol is in invalid format.");
                            }
                        } else {
                            liveContentProtocol = LiveContentWrapper.InvokeMethod(liveContentProtocol);
                            webServiceProtocol.data =
                                    ObjectEncoder.Base64(ObjectEncoder.JSON(liveContentProtocol));
                        }
                        liveContentProtocol.args = ObjectEncoder.Base64(liveContentProtocol.args);
                        liveContentProtocol.returnValue = ObjectEncoder.Base64(liveContentProtocol.returnValue);
                        webServiceProtocol.status = ResponseStatusType.OK.getStatusCode();
                        threadExecutor.Synchronize(() -> resolve(webServiceProtocol));
                    } else {
                        throw new WuiIllegalArgumentException("Method invocation failed.", "Testing protocol is in invalid format.");
                    }
                } else if (webServiceProtocol != null && "AddEventListener".equals(webServiceProtocol.type)) {
                    LiveContentProtocol liveContentProtocol =
                            ObjectDecoder.JSON(ObjectDecoder.Base64(webServiceProtocol.data), LiveContentProtocol.class);
                    if (liveContentProtocol != null) {
                        webServiceProtocol.status = ResponseStatusType.OK.getStatusCode();
                        WebServiceProtocol eventProtocol = webServiceProtocol.clone();
                        LiveContentProtocol eventArgs = liveContentProtocol.clone();
                        EventsManager.getInstance().setEvent(liveContentProtocol.name, (Object... $argument) -> {
                            eventArgs.args = ObjectEncoder.Base64(ObjectEncoder.JSON($argument));
                            eventProtocol.type = "FireEvent";
                            threadExecutor.Synchronize(() -> resolve(eventProtocol));
                        });
                        webServiceProtocol.data = ObjectEncoder.Base64(ObjectEncoder.JSON(true));
                        threadExecutor.Synchronize(() -> resolve(webServiceProtocol));
                    } else {
                        throw new WuiIllegalArgumentException("AddEventListener failed.", "Protocol is in invalid format.");
                    }
                } else {
                    Map webServiceTestProtocol = ObjectDecoder.JSON($data, Map.class);
                    if (webServiceTestProtocol != null) {
                        Object testMessage = webServiceTestProtocol.get("test");
                        if (testMessage != null) {
                            WebServiceProtocol response = new WebServiceProtocol();
                            response.data = "OK";
                            response.clientId = $clientId;
                            threadExecutor.Synchronize(() -> resolve(response));
                        } else {
                            throw new WuiIllegalArgumentException("Method invocation failed.", "Testing protocol is in invalid format.");
                        }
                    } else {
                        throw new WuiIllegalArgumentException("Method invocation failed.", "Protocol is in invalid format.");
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                WebServiceProtocol webServiceProtocol = new WebServiceProtocol();
                webServiceProtocol.clientId = $clientId;
                webServiceProtocol.origin = "http://127.0.0.1/";
                webServiceProtocol.status = ResponseStatusType.INTERNAL_ERROR.getStatusCode();
                webServiceProtocol.data = ObjectEncoder.Base64(ObjectEncoder.JSON(ex.getMessage()));
                webServiceProtocol.type = "Request.Exception";
                threadExecutor.Synchronize(() -> resolve(webServiceProtocol));
            }
        });
    }

    @Override
    public abstract void resolve(WebServiceProtocol $data);
}

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Connectors.Args;

public class ArchiveOptions {
    private String output = null;
    private String type = "";

    public ArchiveOptions Output(final String $output) {
        this.output = $output;
        return this;
    }

    public ArchiveOptions Type(final String $type) {
        this.type = $type;
        return this;
    }

    public String Output() {
        return this.output;
    }

    public String Type() {
        return this.type;
    }
}

/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Connectors;

import Io.Oidis.Jcommons.Exceptions.WuiIOException;
import Io.Oidis.Jcommons.Exceptions.WuiIllegalArgumentException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import static Io.Oidis.Jcommons.Connectors.FileSystemConnector.Exists;

public final class RuntimeConnector {

    private RuntimeConnector() {
    }

    /**
     * Runs executable.
     *
     * @param $path Specify executable path.
     * @throws IOException if execution fails due to an I/O error.
     * @throws IllegalArgumentException if path is null.
     */
    public static void Run(final String $path) throws IOException {
        Run($path, null, false);
    }

    /**
     * Runs executable.
     *
     * @param $path   Specify executable path.
     * @param $params Specify execution parameters
     * @throws IOException if execution fails due to an I/O error.
     * @throws IllegalArgumentException if path is null.
     */
    public static void Run(final String $path, final String $params) throws IOException {
        Run($path, $params, false);
    }

    /**
     * Runs executable.
     *
     * @param $path    Specify executable path.
     * @param $waitFor Specify whether or not the current thread should wait for process to finish.
     * @throws IOException if execution fails due to an I/O error.
     * @throws IllegalArgumentException if path is null.
     */
    public static void Run(final String $path, final boolean $waitFor) throws IOException {
        Run($path, null, $waitFor);
    }

    /**
     * Runs executable.
     *
     * @param $path    Specify executable path.
     * @param $params  Specify execution parameters
     * @param $waitFor Specify whether or not the current thread should wait for process to finish.
     * @throws IOException              if execution fails due to an I/O error.
     * @throws IllegalArgumentException if path is null.
     */
    public static void Run(final String $path, final String $params, final boolean $waitFor) throws IOException {
        if ($path == null) {
            throw new WuiIllegalArgumentException("Error running executable.", "Executable path must not be null.");
        }
        if (Exists($path)) {
            try {
                ProcessBuilder processBuilder = $params == null ? new ProcessBuilder($path) : new ProcessBuilder($path, $params);
                Process process = processBuilder.start();
                if ($waitFor) {
                    process.waitFor();
                }
            } catch (IOException e) {
                throw new WuiIOException("Error starting process.", e);
            } catch (InterruptedException e) {
                throw new WuiIOException("Error while waiting for process to end.", e);
            }
        } else {
            throw new WuiIOException("Error running file : '" + $path + "'.", "File doesn't exist.");
        }
    }

    /**
     * Forces destruction of a process.
     *
     * @param $PID Specify process id.
     * @throws IOException      if destruction fails due to an I/O error.
     * @throws RuntimeException if OS is not supported - supported OS : Windows.
     */
    public static void Destroy(final int $PID) throws IOException {
        if (!System.getProperty("os.name").startsWith("Windows")) {
            throw new RuntimeException(System.getProperty("os.name") + " OS is not supported. Supported OS : Windows.");
        }
        Runtime runtime = Runtime.getRuntime();
        runtime.exec("taskkill /f /PID " + $PID);
    }

    /**
     * Parses and filters process IDs.
     *
     * @param $pattern Specify a pattern by which the processes should be filtered.
     * @return filtered processes.
     * @throws IOException              if acquisition fails due to an I/O error.
     * @throws IllegalArgumentException if passed process name pattern is null.
     * @throws RuntimeException         if OS is not supported - supported OS : Windows.
     */
    public static Set<Integer> getProcessIds(final String $pattern) throws IOException {
        if ($pattern == null) {
            throw new WuiIllegalArgumentException("Error getting IDs of processes.", "Process name pattern must not be null.");
        }
        if (!System.getProperty("os.name").startsWith("Windows")) {
            throw new RuntimeException(System.getProperty("os.name") + " OS is not supported. Supported OS : Windows.");
        }
        try {
            Set<Integer> pids = new HashSet<>();
            String process;
            String cmd = "tasklist.exe /fo csv /nh";
            Process p = Runtime.getRuntime().exec(cmd);
            try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                while ((process = input.readLine()) != null) {
                    String[] processInfo = process.split(",");
                    if (processInfo.length > 0) {
                        String processName = processInfo[0].replaceAll("\"", "");
                        if (processName.matches($pattern)) {
                            pids.add(Integer.valueOf(processInfo[1].replaceAll("\"", "")));
                        }
                    }
                }
            }
            return pids;
        } catch (IOException e) {
            throw new WuiIOException("Error getting IDs of processes.", e);
        }
    }
}

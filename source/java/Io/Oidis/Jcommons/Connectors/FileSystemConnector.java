/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis.Jcommons.Connectors;

import Io.Oidis.Jcommons.Connectors.Args.ArchiveOptions;
import Io.Oidis.Jcommons.Exceptions.WuiIOException;
import Io.Oidis.Jcommons.Exceptions.WuiIllegalArgumentException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public final class FileSystemConnector {

    private static final Logger logger = LogManager.getLogger(FileSystemConnector.class);

    private FileSystemConnector() {
    }

    /**
     * Reads file content as string.
     *
     * @param $path Specify path to file.
     * @return file content.
     * @throws IOException              if reading of file fails due to an I/O error.
     * @throws IllegalArgumentException if passed path is null.
     */
    public static String Read(final String $path) throws IOException {
        if ($path == null) {
            throw new WuiIllegalArgumentException("Error reading file.", "Path cannot be null.");
        }
        try {
            return FileUtils.readFileToString(new File($path), "UTF-8");
        } catch (IOException e) {
            throw new WuiIOException("Error reading file.", e);
        }
    }

    /**
     * Reads resource content as string.
     *
     * @param $path Specify path to resource.
     * @return resource content.
     * @throws IOException              if reading of resource fails due to an I/O error.
     * @throws IllegalArgumentException if passed path is null.
     */
    public static String ReadResource(final String $path) throws IOException {
        if ($path == null) {
            throw new WuiIllegalArgumentException("Error reading resource.", "Path cannot be null.");
        }
        return IOUtils.toString(FileSystemConnector.class.getClassLoader().getResourceAsStream($path),
                "UTF-8");
    }

    /**
     * Writes text into file, creating/overwriting it if needed.
     *
     * @param $path    Specify path to file.
     * @param $content Specify content to be written into file.
     * @throws IOException              if writing of file fails due to an I/O error.
     * @throws IllegalArgumentException if passed path is null.
     */
    public static void Write(final String $path, final String $content) throws IOException {
        if ($path == null) {
            throw new WuiIllegalArgumentException("Error writing file.", "Path cannot be null.");
        }
        try {
            FileUtils.write(new File($path), $content, "UTF-8");
        } catch (IOException e) {
            throw new WuiIOException("Error writing file.", e);
        }
    }

    /**
     * Forces file deletion. If file doesn't exist, method returns successfully.
     *
     * @param $path Specify path to file.
     * @throws IOException if deletion of file fails due to an I/O error.
     */
    public static void Delete(final String $path) throws IOException {
        if (Exists($path)) {
            try {
                FileUtils.forceDelete(new File($path));
            } catch (IOException e) {
                throw new WuiIOException("Error deleting file.", e);
            }
        }
    }

    /**
     * Checks whether or not file exists.
     *
     * @param $path Specify path to the file.
     * @return true/false depending on file existence.
     */
    public static boolean Exists(final String $path) {
        return $path != null && new File($path).exists();
    }

    /**
     * Checks whether or not resource exists.
     *
     * @param $path Specify path to the resource.
     * @return true/false depending on resource existence.
     */
    public static boolean ExistsResource(final String $path) {
        if ($path == null) {
            return false;
        }
        ClassLoader loader = FileSystemConnector.class.getClassLoader();
        try (InputStream resourceStream = loader.getResourceAsStream($path)) {
            return resourceStream != null;
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    /**
     * Extracts file from resources into selected path, overwriting already existing file if needed.
     *
     * @param $resourcePath Specify path to the resource.
     * @param $targetPath   Specify where the file should be extracted.
     * @throws IOException              if extraction fails due to an I/O error.
     * @throws IllegalArgumentException if passed resource or target path is null.
     */
    public static void ExtractJarResource(final String $resourcePath, final String $targetPath) throws IOException {
        if ($resourcePath == null) {
            throw new WuiIllegalArgumentException("Error extracting jar resource.", "Resource path must not be null.");
        } else if ($targetPath == null) {
            throw new WuiIllegalArgumentException("Error extracting jar resource.", "Target path must not be null.");
        }
        new File($targetPath).getParentFile().mkdirs();
        try (InputStream is = FileSystemConnector.class.getClassLoader().getResourceAsStream($resourcePath)) {
            if (is != null) {
                Files.copy(is, Paths.get($targetPath), REPLACE_EXISTING);
            } else {
                throw new IOException("Resource does not exist at jar path : " + $resourcePath);
            }
        } catch (IOException e) {
            throw new WuiIOException("Error extracting jar resource.", e);
        }
    }

    /**
     * Unpacks file at its location.
     *
     * @param $path Specify path to the file to be unpacked.
     * @throws IOException              if unpacking of file fails due to an I/O error.
     * @throws IllegalArgumentException if passed path is null.
     */
    public static void Unpack(final String $path) throws IOException {
        Unpack($path, null);
    }

    /**
     * Unpacks file at specified target location.
     *
     * @param $path    Specify path to the file to be unpacked.
     * @param $options Specify unpacking settings (incl. target location).
     * @throws IOException              if unpacking of file fails due to an I/O error.
     * @throws IllegalArgumentException if passed path is null.
     */
    public static void Unpack(final String $path, final ArchiveOptions $options) throws IOException {
        if ($path == null) {
            throw new WuiIllegalArgumentException("Error unpacking file.", "File path must not be null.");
        }
        if (Exists($path)) {
            String path = $options == null || $options.Output() == null ? $path : $options.Output();
            File target = new File(path.replaceAll("\\.\\w+", ""));
            if (target.isDirectory() && target.exists()) {
                throw new WuiIOException("Error unpacking file : " + $path, "Directory " + target + " already exists.");
            }

            java.util.zip.ZipFile zipFile = null;
            try {
                FileUtils.forceMkdir(target);
                zipFile = new java.util.zip.ZipFile($path);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    File entryTarget = new File(target, entry.getName());
                    if (entry.isDirectory()) {
                        FileUtils.forceMkdir(entryTarget);
                    } else {
                        FileUtils.forceMkdir(entryTarget.getParentFile());
                        try (InputStream in = zipFile.getInputStream(entry);
                             OutputStream out = new FileOutputStream(entryTarget)) {
                            IOUtils.copy(in, out);
                        }
                    }
                }
            } catch (IOException e) {
                throw new WuiIOException("Error unpacking file : " + $path, e);
            } finally {
                if (zipFile != null) {
                    zipFile.close();
                }
            }
        } else {
            throw new WuiIOException("Error unpacking file : " + $path, "File doesn't exist at path : " + $path + ".");
        }
    }
}
